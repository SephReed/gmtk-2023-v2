extends CanvasLayer
class_name DialogueBox

@onready var my_label = $MarginContainer/Panel/Label

var session: DialogueBoxSession


func _ready():
	Globals.state.dialog = self
#	session = DialogueBoxSession.new(self, "res://DialogueTool/Test.dialog.txt")
#	session.start()

#	<guys>: Come on! We've been playing with them for a few weeks now and 

#func display_text(text: String):
#var dialog: DialogueTool.DialogueNode
	
	
func _on_label_meta_clicked(meta):
	session.display_node(meta)
	
	
func new_session(file_path: String):
	return DialogueBoxSession.new(self, file_path)
	
	
class DialogueBoxSession:
	static var TAG = RegEx.create_from_string("<[^>]+>")
	var node: DialogueTool.DialogueNode 
	var parent
	
	func _init(p_in, file_path: String):
		parent = p_in
		node = DialogueTool.open_file(file_path)
		
	func start():
		display_node("main")
		
	func display_node(name: String):
		parent.session = self
		node = node.find_node(name)
		var text = node.get_text()
		text = Globals.str_replace(text, TAG, func(tag: String):
			tag = tag.substr(1, tag.length()-2)
			return "[url=%s]%s[/url]" % [tag, tag]
		)
		parent.display_text(text)
		
#	func find_node(name: String, from: DialogueTool.DialogueNode = node):
#		if (!from):
#			return null;
#		var target = from.get_child(name)
#		if (!target):
#			target = find_node(name, from.parent)
#		return target;
		
		
