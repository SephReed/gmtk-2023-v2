extends CharacterBody2D


@onready var bass_anim = $Attack/Bass/AnimationPlayer;
@onready var camera = $Camera2D;
@onready var attack = $Attack;
#@export var SPEED = 150.0;
@onready var timer_swing = $timer_swing;
@onready var walk_sprite = $WalkSprite;
@onready var walk_anim = $WalkAnim;


func _ready():
	print("Test");
	
func _input(event):
	if event.is_action_pressed("smash"):
		smash();


func _physics_process(delta):
	var speed = Globals.SPEED;
	velocity.x = Input.get_axis("left", "right") * speed;
	velocity.y = Input.get_axis("up", "down") * speed;
	if (velocity.x != 0):
		var scale = -1 if velocity.x <= 0 else 1;
		attack.scale.x = scale;
		walk_sprite.scale.x = -1 * scale;
		walk_anim.play("walk");
	else:
		walk_anim.play("RESET");
		
	move_and_slide()

func smash():
	bass_anim.play("swing");
	timer_swing.start(0.15);
	await timer_swing.timeout;
	attack.play_flash();
	camera.start_shake(0.25,.01,5);
	await bass_anim.animation_finished;	
	bass_anim.play("RESET");
	
	
