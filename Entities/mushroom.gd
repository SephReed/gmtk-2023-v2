extends Node2D

@export var frame = 0;
@onready var sprite = $Sprite2D;
@onready var collision = $Collision;
@export var last_muchroom = false;
@onready var cat_meow = $Catmeow_mp3
@onready var cat_meow2 = $Catmeow2
@onready var cat_meow3 = $Catmeow3
	
func take_damage():
	var rand_meow = randf()
	if rand_meow < 0.333: cat_meow.play();
	elif rand_meow < 0.666: cat_meow2.play();
	else: cat_meow3.play();
		
	sprite.visible = false;
	collision.set_deferred("disabled", true);
	print("WOOO");
	
	
	
	if (last_muchroom):
		Globals.SPEED = 500;
