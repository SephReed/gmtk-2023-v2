class_name DialogueTool



# matches any <name of node>: up to the next named node or end of string
#static var NODE_CHUNK = RegEx.create_from_string("\\t*<[^>]+>:[\\s\\S]+?((?=\\t*<[^>]+>:)|$)")
#static var IF_ELSE_CHUNKS = RegEx.create_from_string("((else)|(if)|(else if))[^:]*:([\\s\\S]*?)(?=((else)|$))")



static func open_file(path: String):
	# Get the raw file contents
	if not FileAccess.file_exists(path): 
		return ERR_FILE_NOT_FOUND

	var file: FileAccess = FileAccess.open(path, FileAccess.READ)
	var raw_text: String = file.get_as_text()
	var out = DialogueNode.new(raw_text)
	print(out)
	return out
	
	
	
	

static func print_tree(node: DialogueNode, depth: int):
	print("\t\t\t\t\t\t".substr(0, depth) + node.name + ": " + node.text)
	for name in node.children:
		print_tree(node.children[name], depth + 1)
	
	
class DialogueNode:
	static var NODE_CHUNK = RegEx.create_from_string(
		"(\\t*)<[^>]+>:[\\s\\S]+?[^\\t]((?=\\1<[^>]+>:)|$)"
	)
	static var IF_ELSE_CHUNKS = RegEx.create_from_string(
		"(else|(else)?\\s*if\\s+([^:]+)):([\\s\\S]*?)(?=(else|$))"
	)
	static var CHUNK_TEXT = RegEx.create_from_string("[\\w\\W]+?((?=<.+>:)|$)")
	static var NODE_NAME = RegEx.create_from_string("<([^>]+)>:")
	static var TABS_AT_FRONT = RegEx.create_from_string("^\\t+")
	var name: String
	var text: String
	var options: Array[DialogueOption]
	var children: Dictionary
	var parent: DialogueNode
	var depth: int
		
	func _init(raw_text: String, i_parent = null):
		var child_text
		if (i_parent == null):
			self.name = "root"
			child_text = raw_text
			self.depth = -1
		else:
			self.parent = i_parent
			var name_and_text = raw_text.split(":", false, 1)
			self.name = NODE_NAME.search(raw_text).strings[1]
			child_text = name_and_text[1]
			var tabs = TABS_AT_FRONT.search(raw_text);
			self.depth = tabs.get_string().length() if tabs else 0
			
			parent.children[self.name] = self
			
			var options = IF_ELSE_CHUNKS.search_all(child_text)
			if options:
				for option in options:
					var condition = option.strings[3]
					var option_text = option.strings[4]
					var add_me = DialogueOption.new(self , condition, option_text) 
					self.options.push_back(add_me)
				return
				
			self.text = CHUNK_TEXT.search(child_text).get_string().strip_edges(true, true)
			
		for result in NODE_CHUNK.search_all(child_text):
			var chunk = result.get_string()
#			print("CHUNK" + chunk)
			var dialog = DialogueNode.new(chunk, self)
			
			
	func get_option():
		if !self.options.size():
			return
		for option in self.options:
			if option.is_valid():
				return option
	
	func find_node(name: String):
		var target = get_child(name)
		if !target && parent:
			target = parent.find_node(name)
		return target
		
	
	func get_child(name: String) -> DialogueNode:
		var option = get_option()
		if option:
			return option.node.get_child(name)
		return children.get(name)
		
	func get_text() -> String:
		var option = get_option()
		if option:
			return option.node.get_text()
		return text
		
	func _to_string():
		var out = name + ":" 
		if parent:
			out = "\t\t\t\t\t\t\t\t".substr(0, depth + 1) + out
			
		if self.options.size():
			for option in options:
				out += "\n" + option.to_string()
			return out
		
		out += "\t\t\t\t\t\t\t\t".substr(0, depth + 1)
		if parent:
			out += text
		for name in children:
			out += "\n" + children[name].to_string()
		return out
		
		
		
		
		
class DialogueOption:
#	var __is_valid = func(): return false
	var node: DialogueNode
	var condition: String
	static var SPLIT_SPACES = RegEx.create_from_string("\\s+")
	
	func _init(parent: DialogueNode, i_condition: String, raw_text: String):
		self.condition = i_condition
		raw_text = "<" + parent.name + ">:" + raw_text
		self.node = DialogueNode.new(raw_text, parent)
#		self.__is_valid = func(): return true
#		print("elif" + raw_text)
		
	func is_valid():
		var parts = condition.strip_edges(true, true).split(" ", false)
		if (parts.size() == 1):
			var out = Globals.state.vars[parts[0]]
			return out
		if (parts.size() == 0):
			return true
		assert("Not setup for condition: " + condition)
		
	
	func _to_string(): 
		var out = "ELIF:"
		out = "\t\t\t\t\t\t\t\t".substr(0, node.parent.depth + 1) + out
		out += node.to_string() + "\n"
		return out
		


