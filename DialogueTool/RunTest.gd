extends Node

const tool = preload("res://DialogueTool/DialogueTool.gd")

#const test_text = "
#<main>:
#I've been watching the <moon> a lot lately.  It keeps me up <all night>.;
#
#r
#	<moon>: Yeah, it's doing something strange.  It twists in ways I've never seen before.
#	<all night>: Well, maybe not all <back:night>.  But I go pretty late.
#
#<night>:
#Lalalala
#"

func _ready():
	var test = RegEx.create_from_string("(.+)=\\1")
	for result in test.search_all("la=la bgbumanbuana=anoga"):
		print(result.get_string())
#	var instance = tool.new();
#	instance.parse_raw_text(test_text)
	DialogueTool.open_file("res://DialogueTool/Test.dialog.txt")
#	print(Globals.str_replace("abcd", RegEx.create_from_string("bc"), func(text: String): return "la"))
