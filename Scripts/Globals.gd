extends Node

var SPEED = 150;



func str_replace(target: String, regex: RegEx, cb: Callable) -> String:
	var out = ""
	var last_pos = 0
	for regex_match in regex.search_all(target):
		var start := regex_match.get_start()
		out += target.substr(last_pos, start - last_pos)
		out += str(cb.call(regex_match.get_string()))
		last_pos = regex_match.get_end()
		
	out += target.substr(last_pos)
	return out
	
	
var state = {
	vars = {
		mushroom_found = true
	}
}

