extends Node

@onready var camera = $"../Player/Camera2D"

var T_HEIGHT = 270
var T_WIDTH = 480

# Called when the node enters the scene tree for the first time.
func _ready():
#	var dpi = DisplayServer.screen_get_dpi()
	var size = DisplayServer.screen_get_size()
	var scale = min(floor(size.x / T_WIDTH), floor(size.y / T_HEIGHT))
	print(scale)
	print(size)
	DisplayServer.window_set_size(Vector2i(scale * T_WIDTH, scale * T_HEIGHT))
	DisplayServer.window_set_position(Vector2i(0,0))
#	ProjectSettings.set_setting("display/window/size/viewport_width", x)
#	ProjectSettings.set_setting("display/window/size/viewport_height", y)
	camera.zoom = Vector2i(scale, scale)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
